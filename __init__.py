from st3m.application import Application, ApplicationContext
from st3m.ui.colours import PUSH_RED, GO_GREEN, BLACK
from st3m.input import InputState
from ctx import Context

import leds
import math

GO_LED = [int(x*255) for x in GO_GREEN]

class Wwaage(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.v_x = 0.0
        self.v_y = 0.0
        self.m_x = False
        self.m_y = False
        self.center = 1.2
 
    def draw(self, ctx: Context) -> None:
        for i in range(40): leds.set_rgb(i, 0, 0, 0)
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        for i in range(7):
            o = 0.5*(i*15+1)
            ctx.rgb(0, 1.0 - i/7, 0)
            ctx.rectangle(-o, -o, o*2, o*2).stroke()
        
        if abs(self.v_x) < self.center: 
            ctx.rgb(*GO_GREEN)
            leds.set_rgb(0,  *GO_LED)
            leds.set_rgb(20, *GO_LED)
        else:        
            ctx.rgb(*PUSH_RED)
        ctx.move_to(self.v_x, -120).line_to(self.v_x, 120).stroke()

        if abs(self.v_y) < self.center: 
            ctx.rgb(*GO_GREEN)
            leds.set_rgb(11, *GO_LED)
            leds.set_rgb(29, *GO_LED)
        else:
            ctx.rgb(*PUSH_RED)
        ctx.move_to(-120, self.v_y).line_to(120, self.v_y).stroke()
        
        leds.update()
        # ctx.fill()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        r_y = ins.imu.acc[0]
        r_x = ins.imu.acc[1]
        z_y = math.sqrt(r_y) * 30 if r_y > 0 else -math.sqrt(-r_y) * 30
        z_x = math.sqrt(r_x) * 30 if r_x > 0 else -math.sqrt(-r_x) * 30
        
        self.v_y += (z_y - self.v_y) / 20
        self.v_x += (z_x - self.v_x) / 20

    def on_exit(self) -> None:
        for i in range(40): leds.set_rgb(i, 0, 0, 0)
        leds.update()

